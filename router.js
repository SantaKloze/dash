// ROUTER VUE

import Vue from 'vue'
import Router from 'vue-router'

import index from '@/pages/index.vue'
import register from '@/pages/register.vue'
import dashboard from '@/pages/dashboard.vue'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        // HOME
        path: '/',
        component: index
      },
      {
        // HOME
        path: '/register',
        component: register
      },
      {
        // HOME
        path: '/dashboard',
        component: dashboard
      }
    ]
  })
}
