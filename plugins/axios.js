export default function ({ $axios, app, store }) {
    $axios.onRequest(config => {
        if (store.state.authToken) {
            config.headers.common['content-type'] = 'application/x-www-form-urlencoded'
        }
    })
}