const mongoose = require('mongoose')

const Schema = mongoose.Schema

const AdviseSchema = new Schema({
    score: Number,
    siret: String,
    comment: String
})

const model = mongoose.model('advise', AdviseSchema)

module.exports = model
